from collections import namedtuple
from textwrap import dedent

import click
from i3ipc import Connection, con


# * Window Class
class Window(Connection):
    # ** dunders
    def __init__(self):
        super(Window, self).__init__()
        self.focused = self.get_tree().find_focused()
        self.workspace = self.focused.workspace().name
        self.rect = self.focused.rect
        self.screen_rect = self.output.rect

    def __str__(self):
        return self.focused.name if self.focused else ""

    # ** properties
    @property
    def floating(self) -> bool:
        if self.focused.floating in ("auto_on", "user_on"):
            return True
        return False

    @property
    def output(self):
        for output in self.get_outputs():
            if output.current_workspace == self.workspace:
                return output

    @property
    def info(self):
        return dedent(
            f"""\
            -----------------------------
            i3window: focused window info
            -----------------------------
            Name:       {self.focused.name}
            Class:      {self.focused.window_class}
            Instance:   {self.focused.window_instance}
            Width:      {self.rect.width}
            Height:     {self.rect.height}
            X position: {self.rect.x}
            Y position: {self.rect.y}
            Floating:   {self.floating}
            Output:     {self.output.name}
            - width:    {self.screen_rect.width}
            - height:   {self.screen_rect.height}
            """
        )

    # ** methods
    def cmd(self, cmd: str) -> None:
        self.focused.command(cmd)

    def toggle_floating(self) -> None:
        opt = "disable" if self.floating else "enable"
        self.cmd(f"floating {opt}")

    def floating_on(self) -> None:
        if not self.floating:
            self.cmd(f"floating enable")

    def floating_off(self) -> None:
        if self.floating:
            self.cmd(f"floating disable")

    def resize(self, ratio: float, *, match_screen: bool = False) -> None:
        window = self.rect
        screen = self.screen_rect

        x = int(screen.width * ratio)
        y = (
            int(screen.height * ratio)
            if match_screen
            else int(x * window.height / window.width)
        )

        self.cmd(f"resize set {x} {y}")

    def center(self) -> None:
        if self.floating:
            self.cmd("move position center")

    def move(self, x: int, y: int) -> None:
        if self.floating:
            self.cmd(f"move position {x} {y}")


# * CLI
# ** Config class
class Config:
    def __init__(self):
        self.window = Window()
        self.reshaped = False
        self.moved = False


pass_config = click.make_pass_decorator(Config, ensure=True)
CONTEXT_SETTINGS = dict(help_option_names=["-h", "--help"])

# ** cli group
@click.group(invoke_without_command=True, chain=True, context_settings=CONTEXT_SETTINGS)
@click.option("-i", "--info", is_flag=True, help="Prints i3 focused window stats")
@click.option(
    "-f",
    "--floating",
    type=click.Choice(["toggle", "on", "off"]),
    help="Toggle floating state",
)
@pass_config
def cli(config, info, floating):
    """
    i3 window management wrapper
    """
    if info:
        click.echo(config.window.info)
    if floating:
        {
            "toggle": config.window.toggle_floating,
            "on": config.window.floating_on,
            "off": config.window.floating_off,
        }[floating]()


# ** resize command
@cli.command()
@click.argument("ratio", default=0.7)
@click.option(
    "-r", "--relative", is_flag=True, help="resize adjusting to screen proportions",
)
@pass_config
def resize(config, ratio, relative):
    """
    Resize window by given ratio (1.0 = 100% of current size, don't resize).
    """
    config.window.resize(ratio, match_screen=relative)


# ** move command
@cli.command()
@click.argument(
    "direction", default="center", type=click.Choice(["nw", "ne", "se", "sw", "center"])
)
@click.option(
    "-p",
    "--padding",
    default=0,
    help="Add padding respectfully to screen borders when moving",
    show_default=True,
)
@click.option(
    "-g",
    "--geometry",
    nargs=2,
    help="Like padding but takes two values: x and y",
    required=False,
)
@pass_config
def move(config, direction: str, padding: int, geometry: tuple) -> None:
    """
    Move window in specified direction
    """
    if config.reshaped:
        click.echo("Window has already been moved")
        quit()

    focused = Window()

    if direction == "center":
        focused.center()
    else:
        screen = focused.screen_rect
        window = focused.rect

        if padding:
            padding_x = padding_y = padding
        else:
            padding_x, padding_y = [int(num) for num in geometry]

        x, y = {
            "nw": (screen.x + padding_x, screen.y + padding_y),
            "sw": (
                screen.x + padding_x,
                screen.y + screen.height - padding_y - window.height,
            ),
            "ne": (screen.width - padding_x - window.width, screen.y + padding_y),
            "se": (
                screen.width - padding_x - window.width,
                screen.y + screen.height - padding_y - window.height,
            ),
        }[direction]

        focused.move(x, y)

    config.moved = True


@cli.command()
@click.argument(
    "direction",
    type=click.Choice(
        ["center", "east", "north", "south", "west", "c", "e", "n", "s", "w", "x", "y"]
    ),
)
@click.option(
    "-r",
    "--ratio",
    default=0.3,
    help="How much of the screen should be covered by reshaped window",
    show_default=True,
)
@pass_config
def shape(config, direction, ratio):
    """
    Shape window according to given ratio and stick it to the border or center of the screen.
    Alternatively -- when x or y args passed -- reshape to square using widht (x) or height (y).
    """
    direction = direction[0:1]

    if config.moved and direction not in ("x", "y"):
        click.echo(
            "Window has already been moved. Don't combine `move` and `combine` commands."
        )
        quit()

    config.reshaped = True

    # c = center
    if direction == "c":
        config.window.resize(0.7, match_screen=True)
        config.window.center()

    elif direction in ("x", "y"):
        side = {"x": config.window.rect.width, "y": config.window.rect.height}[
            direction
        ]
        config.window.cmd(f"resize set {side} {side}")
        config.reshaped = False

    else:
        screen = config.window.screen_rect
        Co = namedtuple("Co", "reshape move")
        Geo = namedtuple("Geo", "x y")

        compass = {
            "n": Co(
                Geo(screen.width, int(screen.height * ratio)), Geo(screen.x, screen.y)
            ),
            "e": Co(
                Geo(int(screen.width * ratio), screen.height),
                Geo(screen.width - int(screen.width * ratio), screen.y),
            ),
            "s": Co(
                Geo(screen.width, int(screen.height * ratio)),
                Geo(screen.x, screen.height - int(screen.height * ratio) + screen.y,),
            ),
            "w": Co(
                Geo(int(screen.width * ratio), screen.height), Geo(screen.x, screen.y)
            ),
        }[direction]

        config.window.cmd(f"resize set {compass.reshape.x} {compass.reshape.y}")

        focused = Window()
        window = focused.rect

        if direction in ("e"):
            difference_x = round((window.width - screen.width * ratio) / 2)
            difference_y = round((window.height - screen.height) / 2)
        elif direction in ("w"):
            difference_x = round((screen.width * ratio - window.width) / 2)
            difference_y = round((window.height - screen.height) / 2)
        elif direction in ("s"):
            difference_x = round((window.width - screen.width) / 2)
            difference_y = round((window.height - screen.height * ratio) / 2)
        elif direction in ("n"):
            difference_x = round((window.width - screen.width) / 2)
            difference_y = round((screen.height * ratio - window.height) / 2)

        focused.move(compass.move.x - difference_x, compass.move.y - difference_y)
