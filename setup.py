from setuptools import setup

setup(
    name="i3window",
    version="0.1",
    py_modules=["i3window", "window"],
    install_requires=["Click", "i3ipc"],
    entry_points="""
    [console_scripts]
    i3window=i3window:cli
    """,
)
